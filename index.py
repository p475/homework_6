# homework 1

print("Run homework 1")

a = int(input("Tell me first side: "))
b = int(input("Tell me second side: "))
c = int(input("Tell me third side: "))


def calculate_area_of_triangle(a: int, b: int) -> None:
    """
    takes 2 args and print area of the triangle
    :param a: int
    :param b: int
    :return: None
    """
    s = (a + b + c) / 2

    area = (s * (s - a) * (s - b) * (s - c)) ** 0.5
    print(f"The area of the triangle {area}")


calculate_area_of_triangle(a, b)

# homework 2

print("Run Homework 2")
text = input("tell me text: ")


def reverse(string: str) -> str:
    """
    takes one args and return reverse text
    :param string: str
    :return: str
    """
    # version 1
    # return string[::-1]

    # version 2
    rev = ''
    n = len(string)

    while n > 0:
        n -= 1
        rev += string[n]

    return rev


print(reverse(text))

# homework 3

print("Run Homework 3")
string = input("tell me text: ")

def number_upper_lower(word: str) -> None:
    """
    takes one args and print number of upper case letters and lower case letters
    :param word: str
    :return:
    """
    upper, lower = 0, 0

    for i in word:
        if i.isupper():
            upper +=1
        elif i.islower():
            lower +=1

    print(f"number of upper case {upper} and number of lower case {lower}")

number_upper_lower(string)

# homework 4
print("Run Homework 4")
string = input("tell me word: ")
def palindrome(word: str) -> bool:
    """
    takes one args and return boolean string is palindrome or not.
    :param word: str
    :return:
    """

    # version 1
    # return word == word[::-1]

    left = 0
    right = len(word)-1
    while left <= right:
        if word[left] != word[right]:
            return False

        left += 1
        right -= 1
    return True


print(palindrome(string))


# homework 5
import sys
import random

print("Run Homework 5")
number = input("write \nrock (write 1) \npaper (write 2) \nscissors (write 3) \n")

if not number.isdigit() or int(number) > 3:
    print('Please enter Number (1, 2, 3)')
    sys.exit()

number = int(number)

def rock_paper_scissors(number: int) -> bool:
    """
    takes one args and return True if rock paper scissors right.
    :param number: int
    :return:
    """
    a = random.randint(1, 3)
    print(a)
    if a == number:
        return True

    return False

print(rock_paper_scissors(number))